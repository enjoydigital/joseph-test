using EnjoyDigitalUniversity.Models;

namespace EnjoyDigitalUniversity.Services
{
    public interface IStudentService
    {
        bool Exists(Student student);

        Student Save(Student student);
    }
}